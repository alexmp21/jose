FROM node:14.17.0

WORKDIR /leoncorp

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run dev

# CMD ["npm", "run", "dev"]