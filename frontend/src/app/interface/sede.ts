export interface SedeListar {
    Filtro : string
}

export interface Sede {
    RUC : string,
    RazonSocial:string,
    Direccion:string,
    Telefono:string,
    Celular:string,
    Guardar:boolean
}
