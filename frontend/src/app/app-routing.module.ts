import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from './forms/menu/menu.component';
import { LoginComponent } from './forms/login/login.component';
import { SedeComponent } from './forms/sede/sede.component';
import { Auth, AuthLogin } from './auth.guard';
import { IniciartramiteComponent } from './forms/iniciartramite/iniciartramite.component';
import { ReportetramiteComponent } from './forms/reportetramite/reportetramite.component';
const routes: Routes = [
  {
    path:'',
    component: LoginComponent,
    pathMatch:'full',
    canActivate:[AuthLogin]
  },
  {
    path:'menu',
    component: MenuComponent,
    canActivate:[Auth]
  },
  {
    path:'sede',
    component: SedeComponent,
    canActivate:[Auth]
  },
  {
    path:'iniciartramite',
    component: IniciartramiteComponent,
    canActivate:[Auth]
  },
  {
    path:'reportetramite',
    component: ReportetramiteComponent,
    canActivate:[Auth]
  },
  { 
    path:'**',
    component:MenuComponent    
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
