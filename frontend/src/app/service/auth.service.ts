import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private URL = environment.endPoint;
  constructor(private http: HttpClient) { }

  iniciarSesion(user: any){
    return this.http.post<any>(this.URL+'iniciosesion',user);
  }
  estadoSesion(){
   const value = localStorage.getItem('sesion');
   const existeToken = !value ? false : !!JSON.parse(value).token;
   return existeToken;
  }
  obtenerLocalStorage(){
   const value = localStorage.getItem('sesion');   
   return JSON.parse(value);
  }
  obtenerToken(){
    const value = localStorage.getItem('sesion');   
    const token = JSON.parse(value).token;
    return { 'Authorization': `bearer ${token}` }
   }
  
}