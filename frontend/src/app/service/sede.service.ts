import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Sede, SedeListar } from '../interface/sede';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SedeService {
  private URL = environment.endPoint;
  constructor(
    private http: HttpClient,
    private auth: AuthService) { }

  listarSede(body: SedeListar){
    const headers = this.auth.obtenerToken();
    return this.http.post<any>(`${this.URL}Sede/ListarSede`, body,{headers}).pipe( map( result => result['result']));
  }
  agregarSede(body: Sede){
    const headers = this.auth.obtenerToken();
    return this.http.post<any>(`${this.URL}Sede/AgregarSede`, body,{headers}).pipe( map( result => result['result']));
  }
}