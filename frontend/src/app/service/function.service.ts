import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import * as CryptoJS from 'crypto-js';
@Injectable({
    providedIn: 'root'
})
export class FunctionService {
    encrypt( text:string ){    
        return CryptoJS.AES.encrypt(text.trim(), environment.key).toString();
    }
    decrypt( text:string ){
       return CryptoJS.AES.decrypt(text.trim(), environment.key).toString( CryptoJS.enc.Utf8);
    }
    getCookie( name:string ){    
        let _name = escape(name);
        const _allCookies = document.cookie;    
        _name += "=";
        const pos = _allCookies.indexOf(_name);    
        if(pos != -1){    
          const start = pos + _name.length;
          let end = _allCookies.indexOf(';', start);
          if(end == -1) end = _allCookies.length;
          const value = _allCookies.substring(start, end);
          return value;
        }    
        return '';
    }
    search(array: any[], column : string, text: string): any[] {
      if (!text) return array;
      return array.filter(item => String(item[column]).toUpperCase().includes(text.toUpperCase()));
    }
    
}