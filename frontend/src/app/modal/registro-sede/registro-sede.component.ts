import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { SedeService } from 'src/app/service/sede.service';

@Component({
  selector: 'app-registro-sede',
  templateUrl: './registro-sede.component.html',
  styleUrls: ['./registro-sede.component.css']
})
export class RegistroSedeComponent implements OnInit {
  Celular : string;
  Direccion: string;
  Ruc: string;
  RazonSocial: string;
  Telefono: string;
  Guardar: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data:any,
    private sSede : SedeService,
    ) { 
    const sede = data.Sede;
    console.log(sede);
    this.Celular = sede.Celular;
    this.Direccion = sede.Direccion;
    this.Ruc = sede.RUC;
    this.RazonSocial = sede.RazonSocial;
    this.Telefono = sede.Telefono;
    this.Guardar = data.Guardar;
  }

  ngOnInit(): void {
  }
  AgregarSede(){   
    const Sede = {
      RUC : this.Ruc,
      RazonSocial:this.RazonSocial,
      Direccion:this.Direccion,
      Telefono:this.Telefono,
      Celular:this.Celular,
      Guardar:this.Guardar
    }
    
    this.sSede.agregarSede(Sede).subscribe(response =>{
      const exito =  response == true;
      if(exito)  {
       console.log('EXITO AL GUARDAR');
      }
    },(e)=>console.log(e))  
  }
}
