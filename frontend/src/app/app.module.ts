import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './forms/menu/menu.component';
import { LoginComponent } from './forms/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SedeComponent } from './forms/sede/sede.component';
import { HttpClientModule } from "@angular/common/http";
import { Auth,AuthLogin } from './auth.guard';
import { RegistroSedeComponent } from './modal/registro-sede/registro-sede.component';
import { MaterialModule } from './materia-module';
import { IniciartramiteComponent } from './forms/iniciartramite/iniciartramite.component';
import { ReportetramiteComponent } from './forms/reportetramite/reportetramite.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    LoginComponent,
    SedeComponent,
    RegistroSedeComponent,
    IniciartramiteComponent,
    ReportetramiteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, 
    ReactiveFormsModule, 
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MaterialModule
  ],
  providers: [
    Auth,
    AuthLogin
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
