import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Sede } from 'src/app/interface/sede';
import { RegistroSedeComponent } from 'src/app/modal/registro-sede/registro-sede.component';
import { AuthService } from 'src/app/service/auth.service';
import { SedeService } from 'src/app/service/sede.service';
import { SedeListar } from '../../interface/sede'
import { FunctionService } from '../../service/function.service';
@Component({
  selector: 'app-sede',
  templateUrl: './sede.component.html',
  styleUrls: ['./sede.component.css']
})
export class SedeComponent implements OnInit {
  form : FormGroup;
  pageSizeOptions: any[]=[50,100]
  displayedColumns: string[] = ['RUC','RazonSocial', 'Direccion', 'Telefono','Celular' ,'acciones' ];
  dataSource = new MatTableDataSource<Sede>([]);
  Filtro : string = "";
  sedeListar: SedeListar;
  Sedes: Sede[];
  constructor(
    private modal: MatDialog,
    private auth:AuthService,
    private fb:FormBuilder,
    private router:Router,    
    private sSede:SedeService,
    private function_:FunctionService
    ) {
      this.listarSede();
      this.crearFormulario();
      
  }
  crearFormulario(){
    this.form = this.fb.group({
       Filtro : [ "", [Validators.required ]],
    });
  }
  
  @ViewChild(MatPaginator)
   paginator!:MatPaginator
  ngAfterViewInit(){
    this.dataSource.paginator = this.paginator
  }

  obtenerLocalStorage(){
    return this.auth.obtenerLocalStorage();
  }

  ngOnInit(): void {

  }
  listarSede(){   
    this.sedeListar = {
      Filtro : this.Filtro
    }

    this.sSede.listarSede(this.sedeListar).subscribe(response =>{
      
      this.pageSizeOptions = [50,100];
      this.dataSource = new MatTableDataSource<Sede>(response);
      this.dataSource.paginator = this.paginator;
      // this.buscarSede();
    },(e)=>console.log(e))  
  }
  
  buscarSede(){
    const busqueda = this.form.value.Filtro;
    this.dataSource = new MatTableDataSource(this.Sedes);
    this.dataSource = new MatTableDataSource( 
      this.function_.search(this.dataSource.data, 'RUC',busqueda)
    );

    const fintro1 = this.dataSource.filteredData.length === 0;
    if(fintro1){
      this.dataSource = new MatTableDataSource( this.Sedes);
      this.dataSource = new MatTableDataSource( 
        this.function_.search( this.dataSource.data, 'RazonSocial',busqueda)
      );
    }   
    this.dataSource.paginator = this.paginator;
  }

  openModal(sede:any) {
    const Guardar = sede.RUC==0?true:false;
    const Sede: any={
      Sede:sede,
      Guardar: Guardar
    }
    this.modal.open(RegistroSedeComponent,{
      data:Sede
    }).afterClosed().subscribe(response => {
      this.listarSede()
    })
  }

}
