import { Component, OnInit } from '@angular/core';
import { NgForm, FormBuilder, FormGroup, Validators,FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { FunctionService } from 'src/app/service/function.service';
import { AuthService } from "../../service/auth.service";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  errorInicioSesion : boolean;
  msjError: string ='';
  user ={
    correo: '',
    password: ''
  }

  form     : FormGroup;
  constructor(
    private router     : Router,
    private fb         : FormBuilder,
    private sfn        : FunctionService,
    private authService: AuthService
  ){
    
    this.crearFormulario();
  }
  crearFormulario(){
    this.form = this.fb.group({
       usuario : [ "", [Validators.required ]],
       contrasena : [ "", [Validators.required ]],
    });
  }
  
  toMenu(){
    this.router.navigateByUrl('menu');
  }
  login(){
    this.authService.iniciarSesion(this.user).subscribe(
      res =>{
        localStorage.setItem('sesion', JSON.stringify(res.result));
        this.errorInicioSesion = false;
        this.msjError = "";
        this.toMenu();
      },
      err =>{
        this.msjError = err.error.message;
        this.errorInicioSesion = true;
      }
    );
  }
  ngOnInit(): void {
  }
}
