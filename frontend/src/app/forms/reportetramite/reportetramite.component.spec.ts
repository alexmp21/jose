import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportetramiteComponent } from './reportetramite.component';

describe('ReportetramiteComponent', () => {
  let component: ReportetramiteComponent;
  let fixture: ComponentFixture<ReportetramiteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportetramiteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportetramiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
