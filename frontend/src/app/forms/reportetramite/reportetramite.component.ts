import { Component, OnInit } from '@angular/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
@Component({
  selector: 'app-reportetramite',
  templateUrl: './reportetramite.component.html',
  styleUrls: ['./reportetramite.component.css']
})
export class ReportetramiteComponent implements OnInit {

  constructor( public matpicker:MatDatepickerModule) { }

  ngOnInit(): void {
  }

}
