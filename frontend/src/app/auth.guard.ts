import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthService } from "./service/auth.service";
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class Auth implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router,
  ) { }
  canActivate(): boolean {
    if (this.authService.estadoSesion())
      return true;
    else {
      this.router.navigateByUrl('');
      return false;
    }
  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthLogin implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router,
  ) { }
  canActivate(): boolean {
    if (this.authService.estadoSesion()) {
      this.router.navigateByUrl('menu', { replaceUrl: true });
    }

    return true;
  }
}

