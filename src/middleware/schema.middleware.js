export const validate = (schema)=>{
    return async(req,res,next)=>{
        try {
            const value = await schema.validateAsync(req.body);
            next();
        } catch (error) {
            res.send(error.message);            
        }
    }
}