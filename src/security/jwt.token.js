const config = require('../security/config');
const passportjwt = require('passport-jwt');    

const opciones = {
    secretOrKey:config.jwt.secreto,
    jwtFromRequest: passportjwt.ExtractJwt.fromAuthHeaderAsBearerToken()
};

module.exports = new passportjwt.Strategy(opciones, async(decriptado, next)=> 
{       
    next(null,{
        val: decriptado.val
    });
});  
