import sql from 'mssql';
require('dotenv').config()
const config = require('../../config/default')

const dbSettings ={
    user: config.connect.user,
    password: config.connect.pass,
    server: config.connect.host,
    database: config.connect.dbase,
    options: {
        encrypt: true, // for azure
        trustServerCertificate: true // change to true for local dev / self-signed certs
      }
}

export const getConnection = async () => {
    try {
      const pool = new sql.ConnectionPool(dbSettings);
      return pool;
    } catch (error) {
      console.error(error);
    }
  };
    
  export async function storedProcedure(getConnection_,nameStoredProcedure,Parameters = [])
  {
      let respuesta = {};

      try
      {
          let conexion = getConnection_;
          await conexion.connect(); 
          let request = await conexion.request();
          Parameters.forEach(
          (params) => 
            {
              request.input(params.parameter,params.value);
            }
          );
          const resultado = await request.execute(nameStoredProcedure);
          
          conexion.close();
          
          respuesta = { 
              status: 200,
              message: 'exito',
              result: resultado

          };
          return respuesta;        
      }
      catch(ex)
      {
          sql.close();

          const excepcion = ex;
          
          respuesta = { 
              status:400,
              message:'¡' + excepcion.message.replace('RequestError','') +'!',
              result:[]
          };
          return respuesta;
      }
  }

  export async function storedProcedureTable(getConnection_,nameStoredProcedure,Parameters = [])
  {
      let respuesta = {};
      try
      {
        let conexion = getConnection_;
        await conexion.connect(); 
          let request = await conexion.request();
          Parameters.forEach(
          (params) => 
            {
              request.input(params.parameter,params.value);
            }
          );
          const resultado = await request.execute(nameStoredProcedure);
          conexion.close();

          respuesta = { 
            status: 200,
            message: 'exito',
            result: resultado.recordset

        };
        return respuesta;      
      }
      catch(ex)
      {
          sql.close();

          const excepcion = ex;

          respuesta = { 
              status:400,
              message:'¡' + excepcion.message.replace('RequestError','') +'!',
              result:[]
          };
          return respuesta;
      }
  }

  export { sql };
