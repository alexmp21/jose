const twilio = require('twilio');
const config = require('../../config/default')

//Envío SMS 
module.exports = (toMessage, toNumber)=>{
    
    const client = new twilio(config.connect.sid_sms,config.connect.tk_sms);

    client.messages.create({
        body: toMessage,
        to: toNumber,
        from: config.connect.phone_number_sms
    })
    .then((message)=> console.log("SMS enviado",toNumber,message.sid))
    .catch(err =>{
        console.log('ERROR ENVIO SMS',err);
    })
}
