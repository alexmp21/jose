const config = require('../../config/default')
const nodemailer = require("nodemailer");

module.exports = (valoresEmail)=>{  
    const { mensajeRecibido, asuntoEmail, cuerpoEmail, correoDestinatario} = valoresEmail;
        
    let transporter = nodemailer.createTransport({
        host: config.connect.hostAccount,
        port: config.connect.port_Email,
        secure: true,
        auth: {
            user: config.connect.account,
            pass: config.connect.passAccount
        }
    });

    const aliascorreo = "TRAMITCORP - MENSAJERIA AUTOMATICA";

    let mailOptions = {
        from:`"${aliascorreo}" ${config.connect.account}`,
        to: correoDestinatario,
        subject: asuntoEmail,
        text: mensajeRecibido,
        html: cuerpoEmail
    };

    // let resEmail = await 
    transporter.sendMail(mailOptions)
    .then((resEmail)=> console.log("Email enviado",resEmail.messageId,' email:',correoDestinatario))
    .catch(err =>{
        console.log('ERROR EMAIL',err);
    });

    return "";
}
