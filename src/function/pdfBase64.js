const config = require('../../config/default')
const axios = require('axios');
import pdf  from 'html-pdf';

module.exports = async (data)=>{  
    const { options, metodo, urlAxios, jsoncuerpo} = data;
        
    const servicio = await axios({
        method: metodo,
        url: urlAxios,
        data: jsoncuerpo
      });      
  
      pdf.create(servicio.data,options).toBuffer(function(err, buffer){
        if(err)
        {
            res.status(400).json({
                status:400,
                message:err,
                result:err
            });    
            return "";
        }
        const base64 = new Buffer.from(buffer).toString('base64')
        
        return base64;
      });
  
    return "";
}
