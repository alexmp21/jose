import { getConnection,storedProcedureTable} from "../database/connection";
import sendEmail from '../function/email'
export const AgregarEncargadoTramite = async (req, res) => {  
    try {
        const {NumeroIdentificacionEncargado,Expediente} = req.body;
        const parameters = [
          {parameter:'NumeroIdentificacionEncargado',value: NumeroIdentificacionEncargado},
          {parameter:'Expediente',value: Expediente}
        ];
        
        const pool = await getConnection();
        const registro = await storedProcedureTable(pool,"SP015_EncargadoTramite_Agregar",parameters);
        
        if(registro.status == 400) {
          res.status(400).json(
            {
                status:400,
                message:'exito',
                result:registro.message
            });
            return;
        } 
        const {EstadoTramite,Correo,Usuario,TipoTramite,CodigoExpediente} = registro.result[0];
        const valoresEmail =
        { 
          mensajeRecibido : 'MENSAJERIA AUTOMÁTICA',
          asuntoEmail:'CAMBIO DE ESTADO TRÁMITE', 
          cuerpoEmail:`<p>Hola ${Usuario}, tu trámite 
                               ${TipoTramite} del Expediente 
                               ${CodigoExpediente} a cambiado de estado a : ${EstadoTramite.toUpperCase() }</p>`, 
          correoDestinatario: Correo
        };
        sendEmail(valoresEmail)

        res.status(200).json({status:200,message:'exito',result:registro.result});
    }
    catch (error) {
      res.status(500).json({status:500,message:'Error: '+error,result:[]});
    }
}