import { getConnection,storedProcedureTable} from "../database/connection";
import sendEmail from '../function/email'
export const AgregarTramite = async (req, res) => {  
    try {
        const {Expediente,NumeroIdentificacionUsuario,IDTipoTramite,Descripcion} = req.body;

        const parameters = [
          {parameter:'NumeroIdentificacionUsuario',value: NumeroIdentificacionUsuario},
          {parameter:'IDTipoTramite',value: IDTipoTramite},
          {parameter:'Descripcion',value: Descripcion}
        ];

        const pool = await getConnection();
        const registro = await storedProcedureTable(pool,"SP014_Tramite_Agregar",parameters);
        
        if(registro.status == 400) {
           res.status(400).json({
            status:400,
            message:registro.message,
            result:registro.result
          });       
            return;
        }   
        res.status(200).json({status:200,message:'exito',result:registro.result});
    }
    catch (error) {
      res.status(500).json({status:500,message:'Error: '+error,result:[]});
    }
}
export const ListarTramite = async (req, res) => {
  try {
      const pool = await getConnection();
      const {FechaInicial,FechaFinal,RUC} = req.body;
      const parameters = [{parameter:'FechaInicial',value: FechaInicial},
                          {parameter:'FechaFinal',value: FechaFinal},
                          {parameter:'RUC',value: RUC},];

      const registro = await storedProcedureTable(pool,"SP016_Tramites_Listar",parameters);
      if(registro.status == 400) 
      {
        res.status(400).json(
          {
              status:400,
              message:'exito',
              result:registro.message
          });
      }
      for(let i = 0; i<registro.result.length; i++){
        registro.result[i].Estados = JSON.parse(registro.result[i].Estados);
      }
      res.status(200).json(
          {
              status:200,
              message:'exito',
              result:registro.result
          });

    } catch (error) {
      res.status(500);
      res.send(error.message);
    }
}
export const AnularTramite = async (req, res) => {  
  try {
      const {Expediente} = req.body;

      const parameters = [
        {parameter:'Expediente',value: Expediente}
      ];
      
      const pool = await getConnection();
      const registro = await storedProcedureTable(pool,"SP017_Tramite_Anular",parameters);
      
      if(registro.status == 400) {
         res.status(400).json({
          status:400,
          message:registro.message,
          result:registro.result
        });       
          return;
      }   

      const {Correo,Usuario,TipoTramite,CodigoExpediente,Encargado} = registro.result[0];
        const valoresEmail =
        { 
          mensajeRecibido : 'MENSAJERIA AUTOMÁTICA',
          asuntoEmail:'CAMBIO DE ESTADO TRÁMITE', 
          cuerpoEmail:`<p>Hola ${Encargado}, el usuario ${Usuario} ANULÓ el proceso del trámite 
                               ${TipoTramite} del Expediente 
                               ${CodigoExpediente}`, 
          correoDestinatario: Correo
        };
        sendEmail(valoresEmail)

      res.status(200).json({status:200,message:'exito',result:registro.result});
  }
  catch (error) {
    res.status(500).json({status:500,message:'Error: '+error,result:[]});
  }
}