import { getConnection,storedProcedureTable} from "../database/connection";
export const AgregarSede = async (req, res) => {  
    try {
        const pool = await getConnection();

        const {RUC,RazonSocial,Direccion,Telefono,Celular,Guardar} = req.body;

        const parameters = [
          {parameter:'RUC',value: RUC},
          {parameter:'RazonSocial',value: RazonSocial},
          {parameter:'Direccion',value: Direccion},
          {parameter:'Telefono',value: Telefono},
          {parameter:'Celular',value: Celular},
          {parameter:'Guardar',value: Guardar}
        ];
        const registro = await storedProcedureTable(pool,"SP003_Sede_Agregar",parameters);
        
        if(registro.status == 400) {
            res.status(400).json(registro);  
            return;
        }   
        res.status(200).json({status:200,message:'exito',result:[]});
    }
    catch (error) {
      res.status(500).json({status:500,message:'Error: '+error,result:[]});
    }
}
export const ListarSede = async (req, res) => {
    try {
        const pool = await getConnection();
        const {Filtro} = req.body;
        const parameters = [{parameter:'Filtro',value: Filtro}];

        const registro = await storedProcedureTable(pool,"SP002_Sede_Listar",parameters);
        if(registro.status == 400) 
        {
            res.status(400).json(registro);
            return;
        }
        res.status(200).json(
            {
                status:200,
                message:'exito',
                result:registro.result
            });

      } catch (error) {
        res.status(500);
        res.send(error.message);
      }
}
export const EliminarSede = async (req, res) => {
    try {
        const pool = await getConnection();
        const {RUC} = req.body;
        const parameters = [{parameter:'RUC',value: RUC}];

        const registro = await storedProcedureTable(pool,"SP004_Sede_Eliminar",parameters);
        if(registro.status == 400) 
        {
            res.status(400).json(registro);
            return;
        } 

        res.status(200).json(
            {
                status:200,
                message:'exito',
                result:registro.result
            });

      } catch (error) {
        res.status(500);
        res.send(error.message);
      }
}