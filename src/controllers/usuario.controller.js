import { getConnection,storedProcedureTable,} from "../database/connection";
const config = require('../security/config');
const jwt = require('jsonwebtoken');
const fs = require('fs')
import morgan from 'morgan'
const path = require('path')
import app from '../app'
import { resolvePlugin } from "@babel/core";
const moment = require('moment')
const axios = require('axios');
const env = require('../../config/default')

export const inicioSesionController = async (req, res) => {
    try {     
        const { correo, password } = req.body;
        const parameters = [
          {parameter:"correo",value: correo},
          {parameter:"password",value: password},
        ];
        const pool = await getConnection();
        const registro = await storedProcedureTable(pool,"SP001_IniciarSesion_Usuario",parameters);

        if(registro.status == 400) 
        { 
          console.log(registro.message);
          res.status(400).json({
            status:400,
            message:registro.message,
            result:registro.result
          });          
          return; 
        }    
        const token =jwt.sign(
          {
            val:"jslogic"
          },
          config.jwt.secreto,
          {
            expiresIn:config.jwt.expirationtime
          }
          );
              

        const {RazonSocial,RUC,NumeroIdentificacion,Nombres,Apellidos} = registro.result[0];
        
        console.log('Inició sesión: ',Nombres,' ',Apellidos);
         
        notificacion();

        res.status(200).json({
          status:200,
          message:'usuario autenticado',
          result: {
            token: token,
            RazonSocial:RazonSocial,
            RUC:RUC,
            NumeroIdentificacion:NumeroIdentificacion,
            Nombres:Nombres,
            Apellidos:Apellidos
          }
        });        
       return;
      } catch (error) {
        res.status(500);
        res.send(error.message.replace('error| RequestError:',''));
      }
}
export const AgregarUsuario = async (req, res) => {  
  try {
      const {NumeroIdentificacion,Correo,RUC,Activo,Nivel,Password,Nombres,Apellidos,Guardar} = req.body;

      const parameters = [
        {parameter:'NumeroIdentificacion',value: NumeroIdentificacion},
        {parameter:'Correo',value: Correo},
        {parameter:'RUC',value: RUC},
        {parameter:'Activo',value: Activo},
        {parameter:'Nivel',value: Nivel},
        {parameter:'Password',value: Password},
        {parameter:'Nombres',value: Nombres},
        {parameter:'Apellidos',value: Apellidos},
        {parameter:'Guardar',value: Guardar},
      ];
      const pool = await getConnection();
      const registro = await storedProcedureTable(pool,"SP011_Usuario_Agregar",parameters);
      
      if(registro.status == 400) {
          res.status(400).json(registro);  
          return;
      }   
      res.status(200).json({status:200,message:'exito',result:[]});
  }
  catch (error) {
    res.status(500).json({status:500,message:'Error: '+error,result:[]});
  }
}
export const ListarUsuario = async (req, res) => {
  try {
      const {Filtro} = req.body;
      const parameters = [{parameter:'Filtro',value: Filtro}];
      const pool = await getConnection();
      const registro = await storedProcedureTable(pool,"SP012_Usuario_Listar",parameters);
      if(registro.status == 400) 
      {
          res.status(400).json(registro);
          return;
      }
      res.status(200).json(
          {
              status:200,
              message:'exito',
              result:registro.result
          });

    } catch (error) {
      res.status(500);
      res.send(error.message);
    }
}
export const EliminarUsuario = async (req, res) => {
  try {
      const {NumeroIdentificacion} = req.body;
      const parameters = [{parameter:'NumeroIdentificacion',value: NumeroIdentificacion}];
      const pool = await getConnection();
      const registro = await storedProcedureTable(pool,"SP013_Usuario_Eliminar",parameters);
      if(registro.status == 400) 
      {
          res.status(400).json(registro);
          return;
      } 

      res.status(200).json(
          {
              status:200,
              message:'exito',
              result:registro.result
          });

    } catch (error) {
      res.status(500);
      res.send(error.message);
    }
}