import { getConnection,storedProcedureTable} from "../database/connection";
export const AgregarTipoTramite = async (req, res) => {  
    try {
        const {IDTipoTramite,Nombre,Observacion,Prefijo,IDEntidadTramite,Guardar} = req.body;

        const parameters = [
          {parameter:'IDTipoTramite',value: IDTipoTramite},
          {parameter:'Nombre',value: Nombre},
          {parameter:'Observacion',value: Observacion},
          {parameter:'Prefijo',value: Prefijo},
          {parameter:'IDEntidadTramite',value: IDEntidadTramite},
          {parameter:'Guardar',value: Guardar}
        ];
        const pool = await getConnection();
        const registro = await storedProcedureTable(pool,"SP008_TipoTramite_Agregar",parameters);
        
        if(registro.status == 400) {
            res.status(400).json(registro);  
            return;
        }   
        res.status(200).json({status:200,message:'exito',result:[]});
    }
    catch (error) {
      res.status(500).json({status:500,message:'Error: '+error,result:[]});
    }
}
export const ListarTipoTramite = async (req, res) => {
    try {
        const pool = await getConnection();
        const {Filtro} = req.body;
        const parameters = [{parameter:'Filtro',value: Filtro}];

        const registro = await storedProcedureTable(pool,"SP009_TipoTramite_Listar",parameters);
        if(registro.status == 400) 
        {
            res.status(400).json(registro);
            return;
        }
        res.status(200).json(
            {
                status:200,
                message:'exito',
                result:registro.result
            });

      } catch (error) {
        res.status(500);
        res.send(error.message);
      }
}
export const EliminarTipoTramite = async (req, res) => {
    try {
        const pool = await getConnection();
        const {IDTipoTramite} = req.body;
        const parameters = [{parameter:'IDTipoTramite',value: IDTipoTramite}];

        const registro = await storedProcedureTable(pool,"SP010_TipoTramite_Eliminar",parameters);
        if(registro.status == 400) 
        {
            res.status(400).json(registro);
            return;
        } 

        res.status(200).json(
            {
                status:200,
                message:'exito',
                result:registro.result
            });

      } catch (error) {
        res.status(500);
        res.send(error.message);
      }
}