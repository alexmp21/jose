import { getConnection,storedProcedureTable} from "../database/connection";
export const AgregarEntidadTramite = async (req, res) => {  
    try {
        const {IDEntidadTramite,NombreEntidad,Direccion,Celular,Correo,Guardar} = req.body;

        const parameters = [
          {parameter:'IDEntidadTramite',value: IDEntidadTramite},
          {parameter:'NombreEntidad',value: NombreEntidad},
          {parameter:'Direccion',value: Direccion},
          {parameter:'Celular',value: Celular},
          {parameter:'Correo',value: Correo},
          {parameter:'Guardar',value: Guardar}
        ];
        const pool = await getConnection();
        const registro = await storedProcedureTable(pool,"SP005_EntidadTramite_Agregar",parameters);
        
        if(registro.status == 400) {
            res.status(400).json(registro);  
            return;
        }   
        res.status(200).json({status:200,message:'exito',result:[]});
    }
    catch (error) {
      res.status(500).json({status:500,message:'Error: '+error,result:[]});
    }
}
export const ListarEntidadTramite = async (req, res) => {
    try {
        const pool = await getConnection();
        const {Filtro} = req.body;
        const parameters = [{parameter:'Filtro',value: Filtro}];

        const registro = await storedProcedureTable(pool,"SP007_EntidadTramite_Listar",parameters);
        if(registro.status == 400) 
        {
            res.status(400).json(registro);
            return;
        }
        res.status(200).json(
            {
                status:200,
                message:'exito',
                result:registro.result
            });

      } catch (error) {
        res.status(500);
        res.send(error.message);
      }
}
export const EliminarEntidadTramite = async (req, res) => {
    try {
        const pool = await getConnection();
        const {IDEntidadTramite} = req.body;
        const parameters = [{parameter:'IDEntidadTramite',value: IDEntidadTramite}];

        const registro = await storedProcedureTable(pool,"SP006_EntidadTramite_Eliminar",parameters);
        if(registro.status == 400) 
        {
            res.status(400).json(registro);
            return;
        } 

        res.status(200).json(
            {
                status:200,
                message:'exito',
                result:registro.result
            });

      } catch (error) {
        res.status(500);
        res.send(error.message);
      }
}