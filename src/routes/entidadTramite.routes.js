import { Router } from "express";
import { AgregarEntidadTramite,ListarEntidadTramite,EliminarEntidadTramite } from "../controllers/entidadTramite.controller";
const {validate} = require('../middleware/schema.middleware');
const schema = require('../schema/entidadTramite.schema');
const passport = require('passport');
const jwtToken = passport.authenticate('jwt',{ session:false });

const router = Router();
router.post('/api/EntidadTramite/AgregarEntidadTramite',[jwtToken,validate(schema.AgregarEntidadTramite)],AgregarEntidadTramite);
router.post('/api/EntidadTramite/ListarEntidadTramite',[jwtToken,validate(schema.ListarEntidadTramite)],ListarEntidadTramite);
router.post('/api/EntidadTramite/EliminarEntidadTramite',[jwtToken,validate(schema.EliminarEntidadTramite)],EliminarEntidadTramite);

export default router;