import { Router } from "express";
import { AgregarEncargadoTramite } from "../controllers/encargadoTramite.controller";
const {validate} = require('../middleware/schema.middleware');
const schema = require('../schema/encargadoTramite.schema');
const passport = require('passport');
const jwtToken = passport.authenticate('jwt',{ session:false });

const router = Router();
router.post('/api/EncargadoTramite/AgregarEncargadoTramite',
            [jwtToken,validate(schema.AgregarEncargadoTramite)],
            AgregarEncargadoTramite);

export default router;