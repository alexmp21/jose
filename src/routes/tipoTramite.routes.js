import { Router } from "express";
import { AgregarTipoTramite,ListarTipoTramite,EliminarTipoTramite } from "../controllers/tipoTramite.controller";
const {validate} = require('../middleware/schema.middleware');
const schema = require('../schema/tipoTramite.schema');
const passport = require('passport');
const jwtToken = passport.authenticate('jwt',{ session:false });

const router = Router();
router.post('/api/TipoTramite/AgregarTipoTramite',[jwtToken,validate(schema.AgregarTipoTramite)],AgregarTipoTramite);
router.post('/api/TipoTramite/ListarTipoTramite',[jwtToken,validate(schema.ListarTipoTramite)],ListarTipoTramite);
router.post('/api/TipoTramite/EliminarTipoTramite',[jwtToken,validate(schema.EliminarTipoTramite)],EliminarTipoTramite);

export default router;