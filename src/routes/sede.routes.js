import { Router } from "express";
import { AgregarSede,ListarSede,EliminarSede } from "../controllers/sede.controller";
const {validate} = require('../middleware/schema.middleware');
const schema = require('../schema/sede.schema');
const passport = require('passport');
const jwtToken = passport.authenticate('jwt',{ session:false });

const router = Router();
router.post('/api/Sede/AgregarSede',[jwtToken,validate(schema.AgregarSede)],AgregarSede);
router.post('/api/Sede/ListarSede',[jwtToken,validate(schema.ListarSede)],ListarSede);
router.post('/api/Sede/EliminarSede',[jwtToken,validate(schema.EliminarSede)],EliminarSede);

export default router;