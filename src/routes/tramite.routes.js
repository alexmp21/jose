import { Router } from "express";
import { AgregarTramite, ListarTramite, AnularTramite } from "../controllers/tramite.controller";
const {validate} = require('../middleware/schema.middleware');
const schema = require('../schema/Tramite.schema');
const passport = require('passport');
const jwtToken = passport.authenticate('jwt',{ session:false });

const router = Router();
router.post('/api/Tramite/AgregarTramite',[jwtToken,validate(schema.AgregarTramite)],AgregarTramite);
router.post('/api/Tramite/ListarTramite',[jwtToken,validate(schema.ListarTramite)],ListarTramite);
router.post('/api/Tramite/AnularTramite',[jwtToken,validate(schema.AnularTramite)],AnularTramite);

export default router;