import { getConnection,storedProcedureTable} from "../src/database/connection";
import morgan from 'morgan'
import config from './config'
import express from 'express'
const path = require('path')
const passport = require('passport')
const cors = require('cors')
const jwtToken = require('./security/jwt.token')
const db = require('./database/Connection');
const fs = require('fs')
//configuración
let puerto = config.port
passport.use(jwtToken)
export const app = express()
app.use(morgan('dev'))
app.use(express.json({limit:'200mb'}))
app.use(passport.initialize())
app.use(cors())
//Motor de plantilla
app.engine('html',require('ejs').renderFile);
app.set("views", path.join(__dirname, "views/document/template"));
app.set("view engine", "ejs");
//Permite obtener ip pública
app.set('trust proxy', true);
//Definir log
var accessLogStream = fs.createWriteStream(path.join(__dirname, '/logs/access.log'), { flags: 'a' })
// configurar log
app.use(morgan('combined', { stream: accessLogStream }))

app.use(express.urlencoded({ extended: false }))
app.use(express.json())

//Routes
import usuarioRoutes from './routes/usuario.routes'; app.use(usuarioRoutes);
import sedeRoutes from './routes/sede.routes'; app.use(sedeRoutes);
import entidadTramiteRoutes from './routes/entidadTramite.routes'; app.use(entidadTramiteRoutes);
import tipoTramiteRoutes from './routes/tipoTramite.routes'; app.use(tipoTramiteRoutes);
import tramiteRoutes from './routes/Tramite.routes'; app.use(tramiteRoutes);
import encargadoTramiteRoutes from './routes/encargadoTramite.routes'; app.use(encargadoTramiteRoutes);
//Servidor
const server = app.listen(puerto, ()=> {
    console.log(`Server: ${puerto}`)
})

export default app