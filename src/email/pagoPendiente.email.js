module.exports = (valoresEmailPago)=>{
    
    const { sedeSolicitante,
            usuarioSolicitante,
            Serial,
            numerodocumentoIdentidad,
            nombreCliente,
            operacion,
            numeroCuenta,
            numeroOperacion,
            Monto}= valoresEmailPago;

    const cuerpo = `<html lang="es">
    <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>...</title>
      <style>
          *{
              margin: 0px;
              padding: 0px;
              border-width: 0px;
          }
          .grupo{
              padding: 1% 3%;
              font-family: Arial, Helvetica, sans-serif;
              width: 360px;
              border: rgb(168, 168, 168) 1px solid;
              border-radius: 10px;
              display: block;
              margin: auto;
          }
          .logo {
              width: 80px;
              text-align: center;
              display: block;
              margin: auto;
              margin-bottom: 4%;
          }
          .solicitud {
              font-size: 10px;
              text-align: center;
              color: rgb(82, 81, 81);
              margin-bottom: 2%;
          }
          .bordes{
              border: rgb(155, 155, 155) 1px solid;
              border-radius: 10px;
              text-align:center;
              padding: 3% 6% 7%;     
              font-size: 16px;
              color: gray;
              margin-bottom: 2%;
          }
          span{
              display: block;
          }
          .tipoprecio{
              font-size: 18px;
              color: rgb(248, 198, 17);
              font-weight: 800;
              padding-bottom: 1%;
          }
          .titulo{
              color:rgb(138, 115, 115);
              font-weight: 900;
              font-size: 10px;
              text-align: center;
              margin-top: 3%;
              margin-bottom: 1%;
          }
          .token{
              background-color:rgb(60, 157, 245);
              color: white;
              border-radius: 10px;
              text-align:center;
              padding: 3%;          
              font-size: 25px;
              width: 70%;
              display: block;
              margin: auto;
              margin-bottom: 5%;
          }
          .descripcion{
              color: gray;
              text-align: center;
              font-size: 14px;
              margin-bottom: 2%;
          }
          .pie{
              color: black;
              text-align: center;
              font-size: 10px;
          }
          .espaciado{
              line-height : 25px;
          }
          .colorAmarillo{
              color: rgb(248, 198, 17);
              font-weight: bold;
          }
          .cajas{
              float: left;
              width: 48%;
              font-size: 14px;
          }
      </style>
  </head>
  
  <body>
      <div class="grupo">
          <img class="logo" src=""http://172.10.20.10/img/logocorp.png">
          <div class="solicitud">
              <h1>SOLICITUD DE CONFIRMACIÓN PAGO ONLINE</h1>
          </div>
  
          <div class="bordes">
              <div class="espaciado">
                  <span>${sedeSolicitante}</span>
                  <span>${usuarioSolicitante}</span>
                  <span class="colorAmarillo">Referencia: ${Serial} </span>
                  <span>N°Doc: ${numerodocumentoIdentidad}</span>
                  <span>Cliente: ${nombreCliente}</span>
                  <span style="Color: green">Monto:S/ ${Monto}</span>
              </div>
          </div>
  
          <div class="bordes">
              <span class="tipoprecio">${operacion}</span>
              <span class="cajas">N° de cuenta:</span>
              <span class="cajas">${numeroCuenta}</span>
          </div>
  
          <div class="titulo">
              <h1>N° de operación:</h1>
          </div>
  
          <div class="token">
              <span>${numeroOperacion}</span>
          </div>
  
          <div class="descripcion">
              <p>
                  Al confirmar el pago online en el reporte de ventas, 
                  brinda su consentimiento para realizar el proceso solicitado, cualquier duda o consulta comunicarse con el área de Control Interno
              </p>
          </div>
  
          <div class="descripcion">
              <span>control@grupomedic.pe</span>
              <span>918197102 / 915247034</span>
          </div>
  
          <div class="pie">
              <p>
                  "Este mensaje de correo electrónico y/o el material adjunto puede contener información confidencial
                  o legalmente protegida por la Ley N° 29733 - Ley de Protección de Datos Personales
                  , y es de uso exclusivo de la(s) persona(s) a quién(es) se dirige. Si no es usted el destinatario
                  indicado,
                  queda notificado de que la lectura, utilización, divulgación y/o copia puede estar prohibida en virtud
                  de la legislación vigente, si usted recibe este mensaje por error
                  por favor notificarlo al remitente y elimine toda la información
              </p>
          </div>
      </div>
  </body>
  
  </html>`;

  return cuerpo;
}