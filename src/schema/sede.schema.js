const joi = require('joi');

const schema  = {};

schema.AgregarSede = joi.object({
    RUC: joi.string().required(),
    RazonSocial: joi.string().required(),
    Direccion: joi.string().required(),
    Telefono: joi.string().length(9).required(),
    Celular: joi.string().length(9).required(),
    Guardar: joi.boolean().required()
});

schema.ListarSede = joi.object({
    Filtro: joi.string().allow('').required(),
});

schema.EliminarSede = joi.object({
    RUC: joi.number().min(10000000000).required(),
});

module.exports = schema;