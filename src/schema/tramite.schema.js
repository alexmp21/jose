const joi = require('joi');
const schema  = {};

schema.AgregarTramite = joi.object({
    NumeroIdentificacionUsuario: joi.string().length(8).required(),
    IDTipoTramite: joi.number().min(1).required(),
    Descripcion: joi.string().allow('').required()
});

schema.ListarTramite = joi.object({
    FechaInicial: joi.string().required(),
    FechaFinal: joi.string().required(),
    RUC: joi.string().allow('').required(),
});

schema.AnularTramite = joi.object({
    Expediente: joi.number().min(1).required()
});

module.exports = schema;