const joi = require('joi');
const schema  = {};

schema.AgregarEncargadoTramite = joi.object({
    NumeroIdentificacionEncargado: joi.string().required(),
    Expediente: joi.number().min(1).required(),
});

module.exports = schema;