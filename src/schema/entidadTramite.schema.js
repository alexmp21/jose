const joi = require('joi');

const schema  = {};

schema.AgregarEntidadTramite = joi.object({
    IDEntidadTramite: joi.number().min(0).required(),
    NombreEntidad: joi.string().required(),
    Direccion: joi.string().required(),
    Celular: joi.string().length(9).required(),
    Correo: joi.string().required(),
    Guardar: joi.boolean().required()
});

schema.ListarEntidadTramite = joi.object({
    Filtro: joi.string().allow('').required(),
});

schema.EliminarEntidadTramite = joi.object({
    IDEntidadTramite: joi.number().min(1).required(),
});

module.exports = schema;