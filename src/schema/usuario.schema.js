const joi = require('joi');

const schema  = {};

schema.inicioSesionSchema = joi.object({
    correo: joi.string().required(),
    password: joi.string().required()
});

schema.AgregarUsuario = joi.object({
    NumeroIdentificacion: joi.string().length(8).required(),
    Correo: joi.string().required(),
    RUC: joi.string().length(11).required(),
    Activo: joi.boolean().required(),
    Nivel: joi.string().required(),
    Password: joi.string().required(),
    Nombres: joi.string().required(),
    Apellidos: joi.string().required(),
    Guardar: joi.boolean().required()
});

schema.ListarUsuario = joi.object({
    Filtro: joi.string().allow('').required(),
});

schema.EliminarUsuario = joi.object({
    NumeroIdentificacion: joi.string().length(8).required(),
});

module.exports = schema;