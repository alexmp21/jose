const joi = require('joi');

const schema  = {};

schema.AgregarTipoTramite = joi.object({
    IDTipoTramite: joi.number().min(0).required(),
    Nombre: joi.string().required(),
    Observacion: joi.string().allow('').required(),
    Prefijo: joi.string().length(4).required(),
    IDEntidadTramite: joi.number().min(1).required(),
    Guardar: joi.boolean().required()
});

schema.ListarTipoTramite = joi.object({
    Filtro: joi.string().allow('').required(),
});

schema.EliminarTipoTramite = joi.object({
    IDTipoTramite: joi.number().min(1).required(),
});

module.exports = schema;