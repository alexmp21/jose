require('dotenv').config()

console.log("*****************************************************");
console.log("TRAMITCORP CONECTADO");
console.log("*****************************************************");

module.exports ={
    connect:{
        host:  process.env.SERVER,
        user:  process.env.USER,
        pass:  process.env.PASS,
        dbase: process.env.DATABASE,
               
        account: process.env.ACCOUNT,
        passAccount: process.env.PASS_ACCOUNT,
        hostAccount: process.env.HOST_ACCOUNT,
        hostApiReniec: process.env.HOST_API_RENIEC,
        hostApiRuc: process.env.HOST_API_RUC,
        tokenApiReniec: process.env.TOKEN_API_RENIEC,        
        port_Email: process.env.PORT_EMAIL        
    }    
}